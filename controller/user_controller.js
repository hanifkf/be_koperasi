const model = require('../models/index');
const {tahun} = require("../utils/date_mapper");
const User = model.user;
const SimpananPokok = model.simpanan_pokok;
const SimpananWajib = model.simpanan_wajib;
const Pinjaman = model.pinjaman;

const getAllUser = async (req, res) => {
    const users = await User.findAll({
        order: [
            ['name','ASC']
        ],
        where: {
            status: 'active'
        },
        include: [
            'simpanan_pokok'
        ]
    }).catch(err=>{
        console.log(err.message)
    });
    res.status(200).send({
        data: users,
        status: true,
        message: 'all user'
    })
}
const getAllNonActiveUser = async (req, res) => {
    const users = await User.findAll({
        order: [
            ['name','ASC']
        ],
        where: {
            status: 'non-active'
        },
        include: [
            'simpanan_pokok'
        ]
    }).catch(err=>{
        console.log(err.message)
    });
    res.status(200).send({
        data: users,
        status: true,
        message: 'all user'
    })
}
const getSingleUser = async (req, res) => {
    await User.findOne({
        include: [
            'simpanan_pokok',
            'pinjaman'
        ],
        where: {
            id: req.params.id
        }
    }).then(result=>{
        res.send({
            data: result,
            status: true
        })
    }).catch(error => {
        res.send(error.message)
    })
}
const addNewUser = async (req, res) => {
    const data = req.body;
    data.tanggal_lahir = new Date(data.tanggal_lahir)
    console.log(data)
    await User.create(data).then(result=>{
        res.send({
            status: true,
            data: result
        })
    }).catch(err=>{
        console.log(err)
        res.send({
            err: err.message
        })
    })
}

const updateUser = async (req, res)=> {
    const data = req.body;
    await User.update(data, {
        where: {
            id: req.params.id
        }
    }).then(row=>{
        if(row>0){
            return res.send({
                status: true,
                message: 'updated'
            })
        }
        res.send({
            status: false,
            message: 'failed'
        })
    })
}

const deleteUser = async (req, res) => {
    await User.destroy({
        where: {
            id: req.params.id
        }
    }).then(async (result)=>{
        if(result){
            await Pinjaman.destroy({
                where: {
                    user_id: req.params.id
                }
            })
            await SimpananPokok.destroy({
                where: {
                    user_id: req.params.id
                }
            })
            await SimpananWajib.destroy({
                where: {
                    user_id: req.params.id
                }
            })
            return res.send({
                status: true,
                message: 'deleted'
            })
        }
        res.send({
            status: false,
            message: 'failed'
        })
    })
}


module.exports = {
    getAllUser,
    getAllNonActiveUser,
    addNewUser,
    updateUser,
    deleteUser,
    getSingleUser
}
