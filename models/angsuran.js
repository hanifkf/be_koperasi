'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class angsuran extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.pinjaman, {
        foreignKey: 'pinjaman_id',
        as: 'pinjaman'
      })
    }
  };
  angsuran.init({
    pinjaman_id: DataTypes.INTEGER,
    angsuran_pokok: DataTypes.FLOAT,
    angsuran_dibayar: DataTypes.FLOAT,
    status: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    angsuran_ke: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'angsuran',
    paranoid: true
  });
  return angsuran;
};
