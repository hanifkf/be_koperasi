const express = require('express');
const router = express.Router();
const userController = require('../controller/user_controller')
const accountController = require('../controller/login_controller')
const authMiddleware = require('../middleware/auth_middleware')

/* GET users listing. */
// router.get('/', userController.getAllUser);
// router.post('/admin/add-new', accountController.addNewAccount)
// router.post('/admin/login', accountController.loginUser)

router.get('/', authMiddleware, userController.getAllUser);
router.post('/admin/add-new', authMiddleware, accountController.addNewAccount)
router.post('/admin/login', accountController.loginUser)
module.exports = router;
