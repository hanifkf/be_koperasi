const express = require('express');
const router = express.Router();
const pinjamanController = require('../controller/pinjaman_controller');
const authMiddleware = require('../middleware/auth_middleware')

//no auth
// router.post('/add/new', pinjamanController.pinjamanBaru);
// router.get('/all', pinjamanController.getAllPinjaman);
// router.get('/all/lunas', pinjamanController.getAllPinjamanLunas);
// router.get('/all/single/:id', pinjamanController.getPinjaman);
// router.get('/angsuran/status/:pinjaman_id', pinjamanController.getStatusAngusran);
// router.get('/user/:user_id', pinjamanController.getPinjamanByUser);
// router.put('/update/:id', pinjamanController.updatePinjaman);
// router.post('/bayar/:id', pinjamanController.bayarAngusran);
// router.post('/bayar-lunas/:id', pinjamanController.bayarLunasPinjaman)


//with auth
router.post('/add/new', authMiddleware, pinjamanController.pinjamanBaru);
router.get('/all', authMiddleware, pinjamanController.getAllPinjaman);
router.get('/all/pending', authMiddleware, pinjamanController.getAllPendingPinjaman);
router.get('/all/lunas', authMiddleware, pinjamanController.getAllPinjamanLunas);
router.get('/all/single/:id', authMiddleware, pinjamanController.getPinjaman);
router.get('/angsuran/status/:pinjaman_id', authMiddleware, pinjamanController.getStatusAngusran);
router.get('/user/:user_id', authMiddleware, pinjamanController.getPinjamanByUser);
router.put('/update/:id', authMiddleware, pinjamanController.updatePinjaman);
router.post('/bayar/:pinjaman_id/:id', authMiddleware, pinjamanController.bayarAngusran);
router.post('/bayar-lunas/:id', authMiddleware, pinjamanController.bayarLunasPinjaman)
router.post('/approve/:pinjaman_id', authMiddleware, pinjamanController.approvePinjaman)
router.get('/pdf/:id', pinjamanController.exportPdf)
router.get('/pdf2/:id', pinjamanController.exportPdf2)
router.get('/schedule', pinjamanController.scheduleTest)
router.get('/rekap/:tahun', pinjamanController.rekapTotalPinjaman)
router.get('/rekap-admin/berjalan/:tahun', pinjamanController.rekapAdministrasiPinjamanBerjalan)
router.get('/rekap-angsuran/berjalan/:tahun', pinjamanController.rekapPemasukanAngsuranBerjalan)
router.get('/rekap-angsuran/lunas/:tahun', pinjamanController.rekapAngsuranPokokLunas)
router.get('/rekap-admin/lunas/:tahun', pinjamanController.rekapBungaAdminLunas)
module.exports = router
