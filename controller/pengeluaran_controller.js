const model = require('../models/index')
const {addLogPengeluaran, updateLogPengeluaran} = require("../utils/log_helper");
const {Sequelize, Op} = require("sequelize");
const moment = require("moment");
require("moment/locale/id")
const Pengeluaran = model.pengeluaran_koperasi;
const TutupBuku = model.tutup_buku;
const SaldoAwal = model.balance_saldo_koperasi;
const addNewPengeluaran = async (req, res) => {
    const data = req.body;
    data.tanggal = moment(data.tanggal)
    console.log(data)
    await Pengeluaran.create(data).then(result=>{
        // addLogPengeluaran('pengeluaran', result.jumlah, result.id)
        res.send({
            status: true,
            data: result,
            message: 'saved'
        })
    }).catch(error => {
        console.log(error)
        res.send({
            status: false,
            message: error.message
        })
    })
}


const updatePengeluaran = async (req, res) =>{
    await Pengeluaran.update(req.body, {
        where:{
            id: req.params.id
        }
    }).then(row=>{
        if(row > 0){
            updateLogPengeluaran('pengeluaran', req.body.jumlah, req.params.id)
            return res.send({
                status: true,
                message: 'updated Pengeluaran'
            })
        }
        res.send({
            status: false,
            message: 'updated failed'
        })

    })
}

const getAllPengeluaran = async (req, res)=>{
    console.log(moment('2021-12-31 17:00:00.000 +00:00').format('LL'))
    const test = await Pengeluaran.findAll({
        where: {
            [Op.and]: {
                createdAt: {
                    [Op.gte] : new Date(`1-1-${new Date().getFullYear().toString()}`),
                    [Op.lte] : new Date(`12-31-${new Date().getFullYear().toString()}`)
                }
            }
        }
    }).catch(err=>{
        console.log(err.message)
    })
    console.log(test)
    await Pengeluaran.findAll({
        where:{
            tahun: new Date().getFullYear().toString()
        },
        order: [
            ['id','DESC']
        ]
    }).then(result=>{
        res.send({
            status: true,
            data: result,
            message: 'all'
        })
    }).catch(err=>{
        res.send(err.message)
    })
}

const rekapPengeluaran = async (req, res) => {
    const tahun = req.params.tahun;
    const rekapPengeluaran = await Pengeluaran.findAll({
        attributes: [
            'bulan',
            'tahun',
            [Sequelize.fn('sum', Sequelize.col('jumlah')), 'total_amount']
        ],
        group: ['bulan', 'tahun'],
        where: {
            tahun: tahun,
        }
    }).catch(err=>{
        console.log(err.message)
    })
    const rekapPengeluaran2 = await Pengeluaran.sum('jumlah' , {
        where: {
            tahun: tahun,
        }
    }).catch(err=>{
        console.log(err)
    })
    res.send({
        rekap1: rekapPengeluaran,
        rekap2: rekapPengeluaran2
    })
}
const getSinglePengeluaran = async (req, res)=>{
    await Pengeluaran.findOne({
        where:{
            id: req.params.id
        }
    }).then(result=>{
        res.send({
            status: true,
            data: result,
            message: 'all'
        })
    }).catch(err=>{
        res.send(err.message)
    })
}
// const getAllPengeluaran = async (req, res)=>{
//     await Pengeluaran.findAll({
//         where:{
//             tahun: new Date().getFullYear().toString()
//         }
//     }).then(result=>{
//         res.send({
//             status: true,
//             data: result,
//             message: 'all'
//         })
//     }).catch(err=>{
//         res.send(err.message)
//     })
// }

const tutupBuku = async (req, res) => {
    const data = req.body;
    const cekData = await TutupBuku.findOne({
        where: {
            tahun: data.tahun
        }
    })
    console.log(cekData)
    if(!cekData){
        await tutupBuku.create(data).then(async (result)=>{
            const saved = {
                jumlah: result.jumlah,
                bulan: 'Januari',
                tahun: `${parseInt(result.tahun) + 1}`
            }
            await SaldoAwal.create(saved)
            res.send({status: true, message: 'Tutup Buku Berhasil'})
        })
    }else {
        res.send({status: false, message: 'tutup buku suda dilakukan'})
    }
}

module.exports = {
    addNewPengeluaran,
    getAllPengeluaran,
    updatePengeluaran,
    getSinglePengeluaran,
    tutupBuku,
    rekapPengeluaran
}
