'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class simpanan_wajib extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.user, {
        foreignKey: 'user_id',
        as: 'user'
      })
    }
  };
  simpanan_wajib.init({
    user_id: DataTypes.INTEGER,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    jumlah: DataTypes.FLOAT,
    status: DataTypes.STRING,
    status_simpanan: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'simpanan_wajib',
    paranoid: true
  });
  return simpanan_wajib;
};
