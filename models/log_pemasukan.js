'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class log_pemasukan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  log_pemasukan.init({
    jumlah: DataTypes.FLOAT,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    type: DataTypes.STRING,
    source_id: DataTypes.INTEGER
  }, {
    sequelize,
    paranoid: true,
    modelName: 'log_pemasukan',
  });
  return log_pemasukan;
};
