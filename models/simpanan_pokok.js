'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class simpanan_pokok extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.user,{
        foreignKey: 'user_id',
        as: 'user'
      })
    }
  };
  simpanan_pokok.init({
    user_id: DataTypes.INTEGER,
    jumlah: DataTypes.FLOAT,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'simpanan_pokok',
    paranoid: true
  });
  return simpanan_pokok;
};
