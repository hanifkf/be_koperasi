const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors')
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const inventarisRouter = require('./routes/inventaris_route')
const anggotaRouter = require('./routes/anggota_route')
const simpananRouter = require('./routes/simpanan_route')
const pengeluaranRouter = require('./routes/pengeluaran_route')
const pinjamanRouter = require('./routes/pinjaman_route')
const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/v1/users', usersRouter);
app.use('/api/v1/inventaris', inventarisRouter);
app.use('/api/v1/anggota', anggotaRouter);
app.use('/api/v1/simpanan', simpananRouter);
app.use('/api/v1/pengeluaran', pengeluaranRouter);
app.use('/api/v1/pinjaman', pinjamanRouter);

module.exports = app;
