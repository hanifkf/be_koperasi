const model = require('../models/index');
const SimpananPokok = model.simpanan_pokok;
const SimpananWajib = model.simpanan_wajib;
const User = model.user;
const {addLogPemasukan, addLogPengeluaran} = require('../utils/log_helper')
const moment = require("moment");
require('moment/locale/id')
const {Sequelize, Op} = require("sequelize");
const addNewSimpananPokok = async (req, res) => {
    const data = req.body;
    await SimpananPokok.create(data).then(result => {
        addLogPemasukan('simpanan_pokok', data.jumlah)
        res.send({
            status: true,
            data: result
        })
    })
}

const getAllSimpananPokok = async (req, res) => {
    await SimpananPokok.findAll({}).then(data => {
        res.send({
            status: true,
            data: data
        })
    })
}

const getAllSimpananPokokByUser = async (req, res) => {
    const userId = req.params.user_id;
    await SimpananPokok.findAll({
        user_id: userId
    }).then(data => {
        res.send({
            status: true,
            data: data
        })
    })
}

const updateSimpananPokok = async (req, res) => {
    const data = req.body;
    await SimpananPokok.update(data, {
        where: {
            id: req.params.id
        }
    }).then(row => {
        if(row > 0) {
            return res.send({
                status: true,
                message: 'updated'
            })
        }
        res.send({
            status: false,
            message: 'failed update'
        })
    })
}

const addNewSimpananWajib = async (req, res) => {
    const data = req.body;
    await SimpananWajib.create(data).then(result => {
        if(result) {
            // addLogPemasukan('simpanan_wajib', data.jumlah, id)
            return res.send({
                status: true,
                message: 'pembayaran simpanan wajib berhasil'
            })
        }
        res.send({
            status: true,
        })
    })
}

const getAllSimpananWajib = async (req, res) => {
    await SimpananWajib.findAll({}).then(data => {
        res.send({
            status: true,
            data: data
        })
    })
}

const getAllSimpananWajibByUser = async (req, res) => {
    const userId = req.params.user_id;
    await SimpananWajib.findAll({
        where: {
            user_id: userId
        },
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        res.send({
            status: true,
            data: data
        })
    })
}

const updateSimpananWajib = async (req, res) => {
    const data = req.body;
    console.log(data)
    await SimpananWajib.update(data, {
        where: {
            id: req.params.id
        }
    }).then(row => {
        if(row > 0) {
            return res.send({
                status: true,
                message: 'updated'
            })
        }
        res.send({
            status: false,
            message: 'failed update'
        })
    }).catch(error => {
        console.log(error.message)
        res.send({
            status: false,
            message: error.message
        })
    })
}

const scheduleTest = async (req, res) => {
    const currentDate = moment(new Date()).format('LL').split([' '])
    console.log(currentDate)
    await User.findAll({
        where: {
            status: 'active'
        }
    }).then(data=>{
        const simpananWajib = {
            bulan: currentDate[1],
            tahun: currentDate[2],
            jumlah: 100000,
            status: 'belum_dibayar',
            status_simpanan: 'active'
        }
        data.map(async (user)=>{
            simpananWajib.user_id = user.id
            console.log(simpananWajib)
            await SimpananWajib.create(simpananWajib)
        })
        res.send({ok: 'ok'})
    })
}

const scheduleSimpananWajib = async () => {
    const currentDate = moment(new Date()).format('LL').split([' '])
    console.log(currentDate)
    await User.findAll({
        where: {
            status: 'active'
        }
    }).then(data=>{
        const simpananWajib = {
            bulan: currentDate[1],
            tahun: currentDate[2],
            jumlah: 100000,
            status: 'belum_dibayar',
            status_simpanan: 'active'
        }
        data.map(async (user)=>{
            simpananWajib.user_id = user.id
            console.log(simpananWajib)
            await SimpananWajib.create(simpananWajib)
        })
        console.log('Schedule Simpanan at, ', moment(new Date()).format('LLLL'))
    })
}

const limitPinjaman = async (req, res) => {
    const userId = req.params.user_id;
    try{
        const totalSimpananWajib = await SimpananWajib.sum('jumlah' , {
            where: {
                user_id: userId,
                status: 'dibayar'
            }
        })
        const totalSimpananPokok = await SimpananPokok.sum('jumlah' , {
            where: {
                user_id: userId,
            }
        })
        res.send({
            status: true,
            data: totalSimpananPokok + totalSimpananWajib
        })
    }catch (e) {
        res.send({
            status: false,
            message: `Error, ${e.message}`
        })
    }
}

const simpananRekap = async (req, res) => {
    const tahun = req.params.tahun;
    const rekapSimpanan1 = await SimpananWajib.findAll({
        attributes: [
            'bulan',
            'tahun',
            'status',
            [Sequelize.fn('sum', Sequelize.col('jumlah')), 'total_amount']
        ],
        group: ['bulan', 'tahun', 'status'],
        where: {
            tahun: tahun,
            status: 'dibayar'
        }
    }).catch(err=>{
        console.log(err.message)
    })
    const rekapSimpanan2 = await SimpananWajib.sum('jumlah' , {
        where: {
            tahun: tahun,
            status: 'dibayar'
        }
    }).catch(err=>{
        console.log(err)
    })
    res.send({
        rekap1: rekapSimpanan1,
        rekap2: rekapSimpanan2
    })
}

const simpananPokokRekap = async (req,res) => {
    const rekapSimpanan2 = await SimpananPokok.sum('jumlah' , {
        where: {
            [Op.and]: {
                createdAt: {
                    [Op.gte] : new Date(`1-1-${new Date().getFullYear().toString()}`),
                    [Op.lte] : new Date(`12-31-${new Date().getFullYear().toString()}`)
                }
            },
            status: 'active'
        }
    }).catch(err=>{
        console.log(err)
    })
    res.send({
        rekap2: rekapSimpanan2
    })
}

module.exports = {
    addNewSimpananPokok,
    addNewSimpananWajib,
    getAllSimpananPokok,
    getAllSimpananPokokByUser,
    getAllSimpananWajib,
    getAllSimpananWajibByUser,
    updateSimpananPokok,
    updateSimpananWajib,
    scheduleTest,
    scheduleSimpananWajib,
    simpananRekap,
    simpananPokokRekap,
    limitPinjaman
}

