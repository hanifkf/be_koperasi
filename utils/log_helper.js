const model = require('../models/index')
const LogPemasukan = model.log_pemasukan;
const LogPengeluaran = model.log_pengeluaran;
const moment = require('moment')
require('moment/locale/id')
const currentDate = moment(Date.now())
const addLogPemasukan = async (source, jumlah, id) => {
    const data = {
        jumlah: jumlah,
        bulan: currentDate.format('MMMM'),
        tahun: currentDate.format('YYYY'),
        type: source,
        source_id: id
    }
    await LogPemasukan.create(data).then(result=>{
        console.log(result)
    })
}

const updateLogPengeluaran = async (source, jumlah, id) => {
    const data = {
        jumlah: jumlah,
        bulan: currentDate.format('MMMM'),
        tahun: currentDate.format('YYYY'),
        type: source,
        source_id: id
    }
    await LogPengeluaran.update(data,{
        where: {
            source: source,
            source_id: id
        }
    }).then(result=>{
        console.log(result)
    })
}

const updateLogPemasukan = async (source, jumlah, id) => {
    const data = {
        jumlah: jumlah,
        bulan: currentDate.format('MMMM'),
        tahun: currentDate.format('YYYY'),
        type: source,
        source_id: id
    }
    await LogPemasukan.update(data,{
        where: {
            source: source,
            source_id: id
        }
    }).then(result=>{
        console.log(result)
    })
}

const addLogPengeluaran = async (source, jumlan, id) => {
    const data = {
        jumlah: jumlah,
        bulan: currentDate.format('MMMM'),
        tahun: currentDate.format('YYYY'),
        type: source,
        source_id: id,
    }
    await LogPengeluaran.create(data).then(result=>{
        console.log(result)
    })
}

const getLogPemasukan = async (source, tahun, bulan) => {
    return await LogPemasukan.findAll({
        where: {
            type: source,
            bulan: bulan,
            tahun: tahun
        }
    })
}

const getLogPengeluaran = async (source, tahun, bulan) => {
    return await LogPengeluaran.findAll({
        where: {
            type: source,
            bulan: bulan,
            tahun: tahun
        }
    })
}

module.exports = {
    addLogPengeluaran,
    addLogPemasukan,
    getLogPemasukan,
    getLogPengeluaran,
    updateLogPengeluaran,
    updateLogPemasukan
}
