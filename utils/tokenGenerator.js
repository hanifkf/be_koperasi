require('dotenv').config()
const jwt = require("jsonwebtoken");


const generateJWTToken = async (data) => {
    try {
        return jwt.sign({data: data}, process.env.TOKEN_AUTH_SECRET,{
            algorithm: "HS256"
        })
    }catch (e) {
        console.log("ERR", e);
    }
}
module.exports = {
    generateJWTToken
}
