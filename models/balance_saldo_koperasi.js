'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class balance_saldo_koperasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  balance_saldo_koperasi.init({
    jumlah: DataTypes.FLOAT,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    modelName: 'balance_saldo_koperasi',
  });
  return balance_saldo_koperasi;
};
