'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasOne(models.simpanan_pokok, {
        foreignKey: 'user_id',
        as: 'simpanan_pokok'
      })
      this.hasMany(models.simpanan_wajib, {
        foreignKey: 'user_id',
        as: 'simpanan_wajib'
      })
      this.hasMany(models.pinjaman,{
        foreignKey: 'user_id',
        as: 'pinjaman'
      })
    }
  };
  user.init({
    name: DataTypes.STRING,
    no_ktp: DataTypes.STRING,
    instansi: DataTypes.STRING,
    phone: DataTypes.STRING,
    status: DataTypes.STRING,
    alamat: DataTypes.TEXT,
    rt_rw: DataTypes.STRING,
    kelurahan: DataTypes.STRING,
    kecamatan: DataTypes.STRING,
    kota_kabupaten: DataTypes.STRING,
    propinsi: DataTypes.STRING,
    email: DataTypes.STRING,
    status_perkawinan: DataTypes.STRING,
    jenis_kelamin: DataTypes.STRING,
    tempat_lahir: DataTypes.STRING,
    tanggal_lahir: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'user',
    paranoid: true,
  });
  return user;
};
