const express = require('express');
const router = express.Router();
const inventarisController = require('../controller/inventaris_controller')
const authMiddleware = require('../middleware/auth_middleware')

/* GET users listing. */

// router.get('/category/all', inventarisController.getMasterCategory);
// router.post('/category/add', inventarisController.addNewCategory)
// router.get('/all', inventarisController.getAllInventaris);
// router.get('/single/:id', inventarisController.getInventaris);
// router.post('/add/new', inventarisController.addNewInventaris);
// router.put('/update/:id', inventarisController.updateInventaris);
// router.delete('/delete/:id', inventarisController.deleteInventaris);


//withauth
router.get('/category/all', authMiddleware, inventarisController.getMasterCategory);
router.post('/category/add', authMiddleware, inventarisController.addNewCategory)
router.delete('/category/delete/:id', authMiddleware, inventarisController.deleteMasterCategory)
router.put('/category/update/:id', authMiddleware, inventarisController.updateMasterCategory)
router.get('/all', authMiddleware, inventarisController.getAllInventaris);
router.get('/single/:id', authMiddleware, inventarisController.getInventaris);
router.post('/add/new', authMiddleware, inventarisController.addNewInventaris);
router.put('/update/:id', authMiddleware, inventarisController.updateInventaris);
router.delete('/delete/:id', authMiddleware, inventarisController.deleteInventaris);
module.exports = router;
