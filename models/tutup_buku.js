'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tutup_buku extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  tutup_buku.init({
    jumlah: DataTypes.FLOAT,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    keterangan: DataTypes.TEXT
  }, {
    sequelize,
    paranoid: true,
    modelName: 'tutup_buku',
  });
  return tutup_buku;
};
