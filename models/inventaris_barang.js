'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class inventaris_barang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.master_kategori_inventaris, {
        foreignKey: 'kategori_id',
        as: 'master_kategori_inventaris'
      })
    }
  };
  inventaris_barang.init({
    nama_barang: DataTypes.STRING,
    kategori: DataTypes.STRING,
    kategori_id: DataTypes.INTEGER,
    biaya: DataTypes.FLOAT,
    keterangan: DataTypes.TEXT,
    tanggal: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'inventaris_barang',
    paranoid: true
  });
  return inventaris_barang;
};
