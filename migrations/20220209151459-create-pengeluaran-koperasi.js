'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('pengeluaran_koperasis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uraian: {
        type: Sequelize.TEXT
      },
      debet: {
        type: Sequelize.FLOAT
      },
      jumlah: {
        type: Sequelize.FLOAT
      },
      kredit: {
        type: Sequelize.FLOAT
      },
      keterangan: {
        type: Sequelize.TEXT
      },
      bulan: {
        type: Sequelize.STRING
      },
      tahun: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('pengeluaran_koperasis');
  }
};
