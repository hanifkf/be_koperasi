const model = require('../models/index');
const MasterCategoryInventaris = model.master_kategori_inventaris;
const Inventaris = model.inventaris_barang;


const addNewCategory = async (req, res)=>{
    // res.send(req.body)
    await MasterCategoryInventaris.create(req.body).then(result=>{
        res.send({
            status: true,
            data: result
        })
    }).catch(error => console.log(error))
}

const getMasterCategory = async (req, res) => {
    await MasterCategoryInventaris.findAll({
        include: ['inventaris_barang'],
        order: [
            ['id', 'DESC']
        ]
    }).then(data=>{
        res.send({
            status: true,
            data: data
        })
    })
}

const updateMasterCategory = async (req, res) => {
    await MasterCategoryInventaris.update(req.body, {
        where: {
            id: req.params.id
        }
    }).then(result=>{
        res.send({
            status: true,
            data: result
        })
    })
}

const deleteMasterCategory = async (req, res) => {
    await MasterCategoryInventaris.destroy({
        where: {
            id: req.params.id
        }
    }).then(result=>{
        res.send({
            status: true,
            data: result
        })
    })
}

const addNewInventaris = async (req, res) => {
    const data = req.body;
    data.tanggal = new Date(data.tanggal)
    console.log(data)
    // res.send("ok")
    await Inventaris.create(req.body).then(result=>{
        res.send({
            status: true,
            data: result
        })
    }).catch(err=>{
        res.send({
            err: err.message
        })
    })
}

const getAllInventaris = async (req, res) => {
    await Inventaris.findAll({
        order: [
            ['id', 'DESC']
        ]
    }).then(data=>{
        res.send({
            status: true,
            data: data
        })
    })
}

const getInventaris = async (req, res) => {
    await Inventaris.findOne({
        where: {
            id: req.params.id
        }
    }).then(data=>{
        res.send({
            status: true,
            data: data
        })
    })
}

const updateInventaris = async (req, res) => {
    await Inventaris.update(req.body, {
        where: {
            id: req.params.id
        }
    }).then(row => {
        if(row > 0) {
            return res.send({
                status: true,
                message: 'updated'
            })
        }
        res.send({
            status: false,
            message: 'failed update'
        })
    }).catch(err=>{
        console.log(err)
        res.send({
            status: false,
            message: err.message
        })
    })
}

const deleteInventaris = async (req, res)=> {
    await Inventaris.destroy({
        where: {
            id: req.params.id
        }
    }).then(result=>{
        if(result){
            return res.send({
                status: true,
                message: 'deleted'
            })
        }
        res.send({
            status: false,
            message: 'failed'
        })
    })
}

module.exports = {
    addNewCategory,
    getMasterCategory,
    addNewInventaris,
    getAllInventaris,
    getInventaris,
    updateInventaris,
    deleteInventaris,
    updateMasterCategory,
    deleteMasterCategory
}
