'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class pinjaman extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.angsuran,{
        foreignKey: 'pinjaman_id',
        as: 'angsuran'
      })
      this.belongsTo(models.user,{
        foreignKey: 'user_id',
        as: 'user'
      })
    }
  };
  pinjaman.init({
    user_id: DataTypes.INTEGER,
    jumlah_diajukan: DataTypes.FLOAT,
    jumlah_diajukan_terbilang: DataTypes.TEXT,
    jumlah_disetujui: DataTypes.FLOAT,
    jumlah_disetujui_terbilang: DataTypes.TEXT,
    keperluan: DataTypes.TEXT,
    administrasi: DataTypes.INTEGER,
    biaya_administrasi: DataTypes.FLOAT,
    bunga: DataTypes.INTEGER,
    jumlah_bunga: DataTypes.FLOAT,
    jumlah_adm_bunga: DataTypes.FLOAT,
    masa_pinjaman: DataTypes.INTEGER,
    angsuran_pokok: DataTypes.FLOAT,
    angsuran_pokok_terbilang: DataTypes.TEXT,
    bunga_per_bulan: DataTypes.FLOAT,
    pinjaman_ke: DataTypes.INTEGER,
    diterima_tanggal: DataTypes.DATE,
    jaminan: DataTypes.STRING,
    status: DataTypes.STRING,
    tahun: DataTypes.STRING,
    status_pengajuan: DataTypes.STRING,
    angsuran_pertama: DataTypes.DATE,
    angsuran_terakhir: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'pinjaman',
    paranoid: true
  });
  return pinjaman;
};
