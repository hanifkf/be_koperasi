const model = require('../models/index')
const bcrypt = require("bcrypt");
const tokenGenerator = require("../utils/tokenGenerator");
const Account = model.account;

const loginUser = async (req, res) => {
    const data = req.body;
    await Account.findOne({
        where: {
            username: data.username
        }
    }).then(async (result) => {
        if (!result) {
            return res.send({
                status: false,
                message: 'Login Failed, Username Not Found'
            })
        } else {
            const passwordCheck = bcrypt.compareSync(data.password, result.password);
            console.log(passwordCheck, 'Password Check')
            if (!passwordCheck) {
                return res.send({
                    status: false,
                    message: 'Login Failed, Password Not Match'
                })
            } else {
                const token = await tokenGenerator.generateJWTToken(result.user_profile)
                await res.send({
                    status: true,
                    message: 'Login Success',
                    user: result,
                    token: token
                })
            }

        }
    })
}

const addNewAccount = async (req, res) => {
    const data = req.body;
    data.password = bcrypt.hashSync(data.password, 10);
    await Account.create(data).then(result => {
        res.send({
            status: true,
            data: result
        })
    })
}

module.exports = {
    addNewAccount,
    loginUser
}
