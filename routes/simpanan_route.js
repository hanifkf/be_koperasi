const express = require('express');
const router = express.Router();
const simpananController = require('../controller/simpanan_controller');
const authMiddleware = require('../middleware/auth_middleware')

//no auth
// router.post('/simpanan-pokok/add', simpananController.addNewSimpananPokok);
// router.post('/simpanan-wajib/add', simpananController.addNewSimpananWajib);
// router.put('/simpanan-pokok/update/:id', simpananController.updateSimpananPokok);
// router.put('/simpanan-wajib/update/:id', simpananController.updateSimpananWajib);
// router.get('/simpanan-pokok/all', simpananController.getAllSimpananPokok);
// router.get('/simpanan-wajib/all', simpananController.getAllSimpananWajib);
// router.get('/simpanan-pokok/user/:user_id', simpananController.getAllSimpananPokokByUser);
router.get('/simpanan-wajib/schedule', simpananController.scheduleTest);


// with auth
router.post('/simpanan-pokok/add', authMiddleware, simpananController.addNewSimpananPokok);
router.post('/simpanan-wajib/add', authMiddleware, simpananController.addNewSimpananWajib);
router.put('/simpanan-pokok/update/:id', authMiddleware, simpananController.updateSimpananPokok);
router.put('/simpanan-wajib/update/:id', authMiddleware, simpananController.updateSimpananWajib);
router.get('/simpanan-pokok/all', authMiddleware, simpananController.getAllSimpananPokok);
router.get('/simpanan-wajib/all', authMiddleware, simpananController.getAllSimpananWajib);
router.get('/simpanan-pokok/user/:user_id', authMiddleware, simpananController.getAllSimpananPokokByUser);
router.get('/simpanan-wajib/user/:user_id', authMiddleware, simpananController.getAllSimpananWajibByUser);
router.get('/simpanan-wajib/rekap/:tahun', simpananController.simpananRekap);
router.get('/simpanan-pokok/rekap/', simpananController.simpananPokokRekap);
router.get('/limit-pinjaman/:user_id', simpananController.limitPinjaman)

module.exports = router
