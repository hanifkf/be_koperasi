'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('pinjamans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      jumlah_diajukan: {
        type: Sequelize.FLOAT
      },
      jumlah_disetujui: {
        type: Sequelize.FLOAT
      },
      administrasi: {
        type: Sequelize.INTEGER
      },
      biaya_administrasi: {
        type: Sequelize.FLOAT
      },
      bunga: {
        type: Sequelize.INTEGER
      },
      jumlah_bunga: {
        type: Sequelize.FLOAT
      },
      jumlah_adm_bunga: {
        type: Sequelize.FLOAT
      },
      masa_pinjaman: {
        type: Sequelize.INTEGER
      },
      angsuran_pokok: {
        type: Sequelize.FLOAT
      },
      bunga_per_bulan: {
        type: Sequelize.FLOAT
      },
      pinjaman_ke: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('pinjamans');
  }
};
