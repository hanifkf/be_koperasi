FROM node:lts-alpine3.15

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
ENV TOKEN_AUTH_SECRET=kapos#@111$$
ENV NODE_ENV=production
COPY . .

EXPOSE 4005

CMD ["npm","run","start-prod"]
