'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class master_kategori_inventaris extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.inventaris_barang, {
        foreignKey: 'kategori_id',
        as: 'inventaris_barang'
      })
    }
  };
  master_kategori_inventaris.init({
    name: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'master_kategori_inventaris',
    paranoid: true
  });
  return master_kategori_inventaris;
};
