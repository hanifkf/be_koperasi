'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class pengeluaran_koperasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  pengeluaran_koperasi.init({
    uraian: DataTypes.TEXT,
    debet: DataTypes.FLOAT,
    jumlah: DataTypes.FLOAT,
    kredit: DataTypes.FLOAT,
    keterangan: DataTypes.TEXT,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    tanggal: DataTypes.DATE
  }, {
    sequelize,
    paranoid: true,
    modelName: 'pengeluaran_koperasi',
  });
  return pengeluaran_koperasi;
};
