const model = require('../models/index')
const Pinjaman = model.pinjaman;
const Angsuran = model.angsuran;
const {addLogPemasukan, addLogPengeluaran} = require('../utils/log_helper')
const {pin} = require("nodemon/lib/version");
const fs = require("fs");
const pdf = require("pdf-creator-node");
const moment = require("moment");
require('moment/locale/id')
const convertRupiah = require('rupiah-format')
const {tanggal} = require("../utils/date_mapper");
const angkaTerbilang = require('@develoka/angka-terbilang-js')
const {formatRupiah} = require("../utils/rupiahFormat");
const {Sequelize} = require("sequelize");
const pinjamanBaru = async (req, res) => {
    const data = req.body;
    const checkUsers = await Pinjaman.findAll({
        where: {
            user_id: data.user_id,
            status_pengajuan: 'disetujui'
        },
        order: [
            ['id', 'ASC']
        ]
    })
    if (checkUsers.length > 0) {
        let lunas = false
        checkUsers.map(pinjaman => {
            if (pinjaman.status === 'lunas') {
                lunas = true
            } else {
                lunas = false
            }
        })
        if (lunas) {
            const pinjamanKe = await Pinjaman.max('pinjaman_ke', {
                where: {
                    user_id: data.user_id
                }
            })
            data.pinjaman_ke = pinjamanKe? pinjamanKe+1 : 1;
            await Pinjaman.create(data).then(result => {
                res.send({
                    status: true,
                    message: 'pinjaman dibuat',
                    data: result
                })
            }).catch(err=>{
                res.send(
                    {
                        status: false,
                        message: err.message
                    }
                )
            })
        } else {
            res.send({
                status: false,
                message: 'user masih punya pinjaman belum lunas'
            })
        }
    } else {
        await Pinjaman.create(data).then(result => {
            res.send({
                status: true,
                message: 'pinjaman dibuat',
                data: result
            })
        })
    }
}

const getAllPinjaman = async (req, res) => {
    await Pinjaman.findAll({
        where: {
            status_pengajuan: 'disetujui'
        },
        include: [
            'user'
        ],
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        res.send({
            status: true,
            data: data,
            message: 'all pinjaman'
        })
    }).catch(err => {
        console.log(err.message)
    })
}

const getAllPendingPinjaman = async (req, res) => {
    await Pinjaman.findAll({
        where: {
            status_pengajuan: 'diajukan'
        },
        include: [
            'user'
        ],
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        res.send({
            status: true,
            data: data,
            message: 'all pinjaman'
        })
    }).catch(err => {
        console.log(err.message)
    })
}

const getAllPinjamanLunas = async (req, res) => {
    await Pinjaman.findAll({
        where: {
            status: 'lunas'
        },
        include: [
            'user'
        ],
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        res.send({
            status: true,
            data: data,
            message: 'all pinjaman'
        })
    })
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
const getPinjaman = async (req, res) => {
    await Pinjaman.findOne({
        where: {
            id: req.params.id
        },
        include: [
            'angsuran',
            'user'
        ]
    }).then(result => {
        console.log(capitalizeFirstLetter(angkaTerbilang(11,5))+ ' rupiah');
        if (result) {
            return res.send({
                status: true,
                data: result,
                message: 'get pinjaman'
            })
        }
        res.send({
            status: false,
            message: 'not found data'
        })
    })
}

const getStatusAngusran = async (req, res) => {
    const pinjamanId = req.params.pinjaman_id;
    await Angsuran.findAll({
        where: {
            pinjaman_id: pinjamanId
        }
    }).then(result => {
        res.send({
            status: true,
            data: result,
            message: 'angsuran'
        })
    })
}

const getPinjamanByUser = async (req, res) => {
    await Pinjaman.findAll({
        where: {
            user_id: req.params.user_id,
            status_pengajuan: 'disetujui'
        },
        include: [
            'angsuran',
            'user'
        ],
        order: [
            ['id', 'DESC']
        ]
    }).then(data => {
        res.send({
            status: true,
            data: data
        })
    }).catch(err => {
        res.send({
            status: false,
            message: err.message
        })
    })
}

const updatePinjaman = async (req, res) => {
    const data = req.body;
    console.log(data)
    await Pinjaman.update(data, {
        where: {
            id: req.params.id
        }
    }).then(row => {
        if (row > 0) {
            return res.send({
                status: true,
                message: 'updated'
            })
        }
        res.send({
            status: false,
            message: 'update failed'
        })
    })
}

const bayarAngusran = async (req, res) => {
    const data = req.body;
    const pinjamanId = req.params.pinjaman_id;
    const id = req.params.id;
    const pinjaman = await Pinjaman.findOne({
        where: {
            id: pinjamanId
        }
    })
    data.user_id = pinjaman.user_id
    data.pinjaman_id = pinjamanId
    if (pinjaman.status === 'lunas') {
        return res.send({
            status: false,
            message: 'pinjaman sudah lunas'
        })
    } else {
        await Angsuran.update(data, {
            where: {
                id: id,
                pinjaman_id: pinjamanId
            }
        }).then(async (result) => {
            const angsuran = await Angsuran.findOne({
                where: {
                    id: id
                }
            })
            if (pinjaman.masa_pinjaman === angsuran.angsuran_ke) {
                await Pinjaman.update({
                    status: 'lunas'
                }, {
                    where: {
                        id: pinjamanId
                    }
                })
                return res.send({
                    status: true,
                    message: 'angsuran dibayar, pinjaman lunas',
                    data: result
                })
            } else {
                res.send({
                    status: true,
                    message: `angsuran dibayar,`,
                    data: result
                })
            }
        })
    }
}

const bayarLunasPinjaman = async (req, res) => {
    const pinjamanId = req.params.id;
    const dataPinjaman = await Pinjaman.findOne({
        where: {
            id: pinjamanId
        }
    })
    if (dataPinjaman.status === 'lunas') {
        return res.send({
            status: false,
            message: 'pinjaman sudah lunas'
        })
    } else {
        await Pinjaman.update({
            status: 'lunas'
        }, {
            where: {
                id: pinjamanId
            }
        }).then(async (row) => {
            if (row > 0) {
                let angusranKe = await Angsuran.max('angsuran_ke', {
                    where: {
                        pinjaman_id: pinjamanId
                    }
                })
                const selisih = dataPinjaman.masa_pinjaman - angusranKe
                for (let i = 0; i < selisih; i++) {
                    angusranKe += 1
                    const dataAngusran = {
                        pinjaman_id: pinjamanId,
                        angsuran_pokok: dataPinjaman.angsuran_pokok,
                        angsuran_dibayar: dataPinjaman.angsuran_pokok + dataPinjaman.bunga_per_bulan,
                        status: 'diterima',
                        user_id: dataPinjaman.user_id,
                        angsuran_ke: angusranKe
                    }
                    await Angsuran.create(dataAngusran)
                    addLogPemasukan('angsuran',)
                }
                return res.send({
                    status: true,
                    message: 'pelunasan diterima'
                })
            } else {
                return res.send({
                    status: false,
                    message: 'pelunasan gagal'
                })
            }
        })
    }
}

const approvePinjaman = async (req, res) => {
    const pinjamanId = req.params.pinjaman_id;
    console.log(pinjamanId)
    const data = req.body;
    data.status_pengajuan = 'disetujui';
    console.log(data)
    //add logs pemasukan admin
    //add logs pengeluaran pinjaman
    await Pinjaman.update(data, {
        where: {
            id: pinjamanId
        }
    }).then(row => {
        if (row > 0) {
            return res.send({
                status: true,
                message: 'approve done'
            })
        }
        res.send({
            status: false,
            message: 'approve failed'
        })
    }).catch(err=>{
        console.log(err)
    })
}

const exportPdf = async (req, res) => {
    const id = req.params.id;
    let html = fs.readFileSync(__dirname + "/surat1.html", "utf8");
    let bitmap = fs.readFileSync(__dirname + "/img.png")
    const logo = bitmap.toString('base64')
    const data = await Pinjaman.findOne({
        where: {
            id: id
        },
        include: [
            'user'
        ]
    })
    let currentDate = moment(new Date()).format('LL')
    const newCurrentDate = currentDate;
    console.log(currentDate.toString().split(' '))
    currentDate = currentDate.toString().split(' ')
    const user = data.user;
    const options = {
        format: "A4",
        orientation: "portrait",
        marginTop: "100mm",
        // header: {
        //     height: "15mm",
        // },
        // footer: {
        //     height: "15mm",
        // },
    };
    const document = {
        html: html,
        data: {
            users: [],
            logo: logo,
            no_anggota: user.id,
            instansi: user.instansi,
            name: user.name,
            no_ktp: user.no_ktp,
            ttl: `${user.tempat_lahir}, ${moment(user.tanggal_lahir).format('LL')}`,
            jenis_kelamin: user.jenis_kelamin,
            status_perkawinan: user.status_perkawinan,
            alamat: user.alamat,
            kelurahan: user.kelurahan,
            kecamatan: user.kecamatan,
            kota: user.kota_kabupaten,
            no_telp: user.phone,
            email: user.email,
            jumlah_permintaan: formatRupiah(data.jumlah_diajukan),
            keperluan: data.keperluan,
            admin: data.administrasi,
            pinjaman_disetujui: formatRupiah(data.jumlah_disetujui),
            diterima_tanggal: newCurrentDate,
            bunga: data.bunga,
            angsuran_per_bulan: formatRupiah(data.angsuran_pokok),
            masa: data.masa_pinjaman,
            jaminan: data.jaminan,
            tanggal_lunas: moment(data.angsuran_terakhir).format('LL')
        },
        path: "./output.pdf",
        type: "buffer",
    };

    await pdf.create(document, options)
        .then((response) => {
            res.setHeader(
                "Content-disposition",
                `inline; filename="${data.user.name}-FORM-PINJAMAN.pdf"`
            );
            res.setHeader("Content-type", "application/pdf");
            return res.send(response);
        })
        .catch((error) => {
            console.error(error);
        });
}

const exportPdf2 = async (req, res) => {
    const id = req.params.id;
    let html = fs.readFileSync(__dirname + "/surat2.html", "utf8");
    let bitmap = fs.readFileSync(__dirname + "/img.png")
    const logo = bitmap.toString('base64')

    const data = await Pinjaman.findOne({
        where: {
            id: id
        },
        include: [
            'user'
        ]
    })
    let currentDate = moment(new Date()).format('LLLL')
    console.log(currentDate.toString().split(' '))
    currentDate = currentDate.toString().split(' ')

    const angsuranPertama = moment(data.angsuran_pertama).format('LLLL').split([' '])
    const angsuranTerakhir = moment(data.angsuran_terakhir).format('LLLL').split([' '])
    console.log(angsuranPertama)
    console.log(angsuranTerakhir)
    const options = {
        format: "A4",
        orientation: "portrait",
        marginTop: "100mm",
        // header: {
        //     height: "15mm",
        // },
        // footer: {
        //     height: "15mm",
        // },
    };
    const document = {
        html: html,
        data: {
            users: [],
            logo: logo,
            nomer: `02`,
            tahun: currentDate[3],
            hari: currentDate[0],
            nama: data.user.name.toUpperCase(),
            tanggal_word: tanggal[currentDate[1]],
            bulan: currentDate[2],
            bunga: data.bunga,
            no_anggota: '02',
            jumlah: formatRupiah(data.jumlah_disetujui),
            bunga_per_bulan: formatRupiah(data.bunga_per_bulan),
            angsuran_pokok: formatRupiah(data.angsuran_pokok),
            masa_pinjaman: data.masa_pinjaman,
            bulan_pertama: angsuranPertama[2],
            bulan_terakhir: angsuranTerakhir[2],
            tahun_akhir: angsuranTerakhir[3],
            tahun_pertama: angsuranPertama[3],
            jumlah_bayar: formatRupiah(data.angsuran_pokok + data.bunga_per_bulan),
            terbilang: data.angsuran_pokok_terbilang,
            current_date: moment(new Date()).format('LL')
        },
        path: "./output.pdf",
        type: "buffer",
    };

    await pdf.create(document, options)
        .then((response) => {
            res.setHeader(
                "Content-disposition",
                `inline; filename="${data.user.name}-SPH.pdf"`
            );
            res.setHeader("Content-type", "application/pdf");
            return res.send(response);
        })
        .catch((error) => {
            console.error(error);
        });
}

const scheduleTest = async (req, res) => {
    const pinjamanJalan = await Pinjaman.findAll({
        where: {
            status: 'belum_lunas',
            status_pengajuan: 'disetujui'
        },
        include: [
            'user'
        ]
    })
    pinjamanJalan.map(async (data) => {
        let angsuranKe = await Angsuran.max('angsuran_ke', {
            where: {
                pinjaman_id: data.id
            }
        })
        angsuranKe += 1;
        console.log(angsuranKe)
        console.log(data.masa_pinjaman)
        if(angsuranKe <= data.masa_pinjaman){
            console.log('TESSS')
            const saved = {
                pinjaman_id: data.id,
                angsuran_pokok: data.angsuran_pokok,
                angsuran_dibayar: data.angsuran_pokok + data.bunga_per_bulan,
                status: 'belum_dibayar',
                user_id: data.user.id,
                angsuran_ke: angsuranKe
            }
            console.log(saved)
            // await Angsuran.create(saved)
        }
    })
    console.log(pinjamanJalan)
    res.send({ok: pinjamanJalan})
}
const scheduleAngsuran = async () => {
    const pinjamanJalan = await Pinjaman.findAll({
        where: {
            status: 'belum_lunas',
            status_pengajuan: 'disetujui'
        },
        include: [
            'user'
        ]
    })
    console.log(pinjamanJalan)
    pinjamanJalan.map(async (data) => {
        let angsuranKe = await Angsuran.max('angsuran_ke', {
            where: {
                pinjaman_id: data.id
            }
        })
        angsuranKe += 1;
        console.log(angsuranKe)
        console.log(data.masa_pinjaman)
        if(angsuranKe <= data.masa_pinjaman){
            console.log('TESSS')
            const saved = {
                pinjaman_id: data.id,
                angsuran_pokok: data.angsuran_pokok,
                angsuran_dibayar: data.angsuran_pokok + data.bunga_per_bulan,
                status: 'belum_dibayar',
                user_id: data.user.id,
                angsuran_ke: angsuranKe
            }
            console.log(saved)
            await Angsuran.create(saved)
        }
    })
    // console.log(pinjamanJalan)
    console.log('Schedule Angsuran at, ', moment(new Date()).format('LLLL'))

}

const rekapTotalPinjaman = async (req, res) => {
    const {tahun} = req.params;
    const rekap = await Pinjaman.sum('jumlah_disetujui', {
        where: {
            tahun: tahun
        }
    }).catch(err=>{
        console.log(err)
    })
    res.send({
        data: rekap
    })
}

const rekapAdministrasiPinjamanBerjalan = async (req, res) => {
    const {tahun} = req.params;
    const rekap = await Pinjaman.sum('biaya_administrasi', {
        where: {
            tahun: tahun,
            status: 'belum_lunas'
        }
    })
    res.send({
        data: rekap
    })
}

const rekapPemasukanAngsuranBerjalan = async (req, res) => {
    //dengan bunga
    const tahun = req.params.tahun;
    // const rekap = await Pinjaman.findAll({
    //     include: [
    //         {
    //             model: Angsuran,
    //             as: 'angsuran',
    //             // attributes: [
    //             //     'pinjaman_id',
    //             //     [Sequelize.fn('sum', Sequelize.col('angsuran_dibayar')), 'total_amount']
    //             // ],
    //             // group: ['pinjaman_id'],
    //             where: {
    //                 status: 'dibayar'
    //             }
    //         }
    //     ],
    //     where: {
    //         tahun: tahun
    //     }
    // }).catch(err=>{
    //     console.log('ERRR, ',err.message)
    // })
    const rekap = await Angsuran.sum('angsuran_dibayar',{
        // attributes: [
        //     [Sequelize.fn('sum', Sequelize.col('angsuran_dibayar')), 'total_amount']
        // ],
        include: [
            {
                model: Pinjaman,
                as: 'pinjaman',
                attributes: [],
                // required: true,
                where: {
                    tahun: tahun,
                    status: 'belum_lunas'
                }
            }
        ],
        // group: ['angsuran.id'],
        where: {
            status: 'dibayar'
        }
    }).catch(err=>{
            console.log('ERRR, ',err.message)
        })
    res.send({
        data: rekap
    })
}

const rekapAngsuranPokokLunas = async (req, res) => {
    //tanpa bunga
    const tahun = req.params.tahun;
    const rekap = await Angsuran.sum('angsuran.angsuran_pokok',{
        // attributes: [
        //     [Sequelize.fn('sum', Sequelize.col('angsuran_dibayar')), 'total_amount']
        // ],
        include: [
            {
                model: Pinjaman,
                as: 'pinjaman',
                attributes: [],
                // required: true,
                where: {
                    tahun: tahun,
                    status: 'lunas'
                }
            }
        ],
        // group: ['angsuran.id'],
        where: {
            status: 'dibayar'
        }
    }).catch(err=>{
        console.log('ERRR, ',err.message)
    })
    res.send({
        data: rekap
    })
}

const rekapBungaAdminLunas = async (req, res) => {
    const {tahun} = req.params;
    const rekapBungaAdmin = await Pinjaman.sum('jumlah_adm_bunga',{
        where: {
            tahun: tahun,
            status: 'lunas'
        }
    })
    res.send({
        data: rekapBungaAdmin
    })
}
// const rekapAdminBunga Lunas
module.exports = {
    getStatusAngusran,
    getAllPinjaman,
    pinjamanBaru,
    getPinjamanByUser,
    getPinjaman,
    updatePinjaman,
    getAllPinjamanLunas,
    bayarAngusran,
    bayarLunasPinjaman,
    exportPdf,
    exportPdf2,
    approvePinjaman,
    scheduleTest,
    scheduleAngsuran,
    getAllPendingPinjaman,
    rekapTotalPinjaman,
    rekapAdministrasiPinjamanBerjalan,
    rekapPemasukanAngsuranBerjalan,
    rekapAngsuranPokokLunas,
    rekapBungaAdminLunas
}
