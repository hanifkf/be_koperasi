const express = require('express');
const router = express.Router();
const userController = require('../controller/user_controller');
const authMiddleware = require('../middleware/auth_middleware')
//with auth
router.post('/add/new', authMiddleware, userController.addNewUser);
router.put('/update/:id', authMiddleware,userController.updateUser);
router.get('/all', authMiddleware,userController.getAllUser);
router.get('/all/nonactive', authMiddleware,userController.getAllNonActiveUser);
// router.get('/single/:id', authMiddleware,userController.getSingleUser);
router.delete('/delete/:id', authMiddleware,userController.deleteUser)

//no auth
// router.post('/add/new', userController.addNewUser);
// router.put('/update/:id', userController.updateUser);
// router.get('/all', userController.getAllUser);
router.get('/single/:id', userController.getSingleUser);
// router.delete('/delete/:id', userController.deleteUser)
module.exports = router
