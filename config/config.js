require('dotenv').config()
module.exports = {
    development: {
        username: process.env.USERNAME_DB,
        password: process.env.PASSWORD_DB,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        port: 5432,
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    test: {
        username: "root",
        password: null,
        database: "database_test",
        host: "127.0.0.1",
        dialect: "mysql"
    },
    production: {
        username: 'postgres',
        password: 'hanifkf12',
        database: 'kapos',
        host: '103.183.74.203',
        port: 5432,
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
}
