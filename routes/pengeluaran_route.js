const express = require('express');
const router = express.Router();
const pengeluaranController = require('../controller/pengeluaran_controller');
const authMiddleware = require('../middleware/auth_middleware')

//with auth
router.get('/all', authMiddleware, pengeluaranController.getAllPengeluaran);
router.get('/single/:id', authMiddleware, pengeluaranController.getSinglePengeluaran);
router.put('/update/:id', authMiddleware, pengeluaranController.updatePengeluaran);
router.post('/add/new', authMiddleware, pengeluaranController.addNewPengeluaran)
router.get('/rekap/:tahun', authMiddleware, pengeluaranController.rekapPengeluaran)

//no auth
// router.get('/all', pengeluaranController.getAllPengeluaran);
// router.put('/update/:id', pengeluaranController.updatePengeluaran);
// router.post('/add/new', pengeluaranController.addNewPengeluaran)

module.exports = router
