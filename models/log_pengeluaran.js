'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class log_pengeluaran extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  log_pengeluaran.init({
    jumlah: DataTypes.FLOAT,
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    type: DataTypes.STRING,
    source_id: DataTypes.INTEGER
  }, {
    sequelize,
    paranoid: true,
    modelName: 'log_pengeluaran',
  });
  return log_pengeluaran;
};
